# This is TUX's Dockerfile!
FROM alpine

LABEL maintainer="pety.barczi@gmail.com"

RUN apk add --no-cache bash

COPY ./app /app

CMD /bin/bash /app/tux.sh
